#pragma once
#include <stdint.h>
#include <cctype>

namespace bwn
{
namespace str_parsing
{

enum class EResult
{
    k_ok,
    k_overflow,
    k_underflow,
    k_invalid
};

template<typename CharT>
EResult asDecimalStrict(const CharT*const p_begin, const CharT*const p_end, int64_t& o_result)
{
    if (p_begin >= p_end)
    {
        return EResult::k_invalid;
    }

    const CharT firstCharacter = *p_begin;
    const bool isNegative = firstCharacter == '-';    
    const bool hasSign = firstCharacter == '-' || firstCharacter == '+';

    if (p_begin + hasSign == p_end)
    {
        return EResult::k_invalid;
    }

    static constexpr int64_t maxInt = (~static_cast<uint64_t>(0)) >> 1;
    static constexpr int64_t maxIntDiv = maxInt / 10;
    static constexpr int64_t maxAddition = maxInt - (maxIntDiv * 10);

    int64_t accumulator = 0;
    for (const CharT* charIt = p_begin + hasSign; charIt < p_end; ++charIt)
    {
        const CharT character = *charIt;

        if (!std::iswdigit(static_cast<wchar_t>(character)))
        {
            return EResult::k_invalid;
        }

        const int64_t addition = character - '0';

        if (accumulator > maxIntDiv || (accumulator == maxIntDiv && addition > maxAddition ) )
        {
            return isNegative ? EResult::k_underflow : EResult::k_overflow;
        }

        accumulator *= 10;
        accumulator += addition;
    }

    o_result = isNegative 
		? -static_cast<int64_t>(accumulator)
		: static_cast<int64_t>(accumulator);
    return EResult::k_ok;
}

template<typename CharT>
EResult asDecimalStrict(const CharT*const p_begin, const CharT*const p_end, uint64_t& o_result)
{
    if (p_begin >= p_end)
    {
        return EResult::k_invalid;
    }

    static constexpr uint64_t maxInt = ~static_cast<uint64_t>(0);
    static constexpr uint64_t maxIntDiv = maxInt / 10;
    static constexpr uint64_t maxAddition = maxInt - (maxIntDiv * 10);

    uint64_t accumulator = 0;
    for (const CharT* charIt = p_begin; charIt < p_end; ++charIt)
    {
        const CharT character = *charIt;

        if (!std::iswdigit(static_cast<wchar_t>(character)))
        {
            return EResult::k_invalid;
        }

        const uint64_t addition = character - '0';

        if (accumulator > maxIntDiv || (accumulator == maxIntDiv && addition > maxAddition) )
        {
            return EResult::k_overflow;
        }

        accumulator *= 10;
        accumulator += addition;
    }

    o_result = accumulator;
    return EResult::k_ok;
}

template<typename CharT>
EResult asHexadecimalBodyStrict(const CharT*const p_begin, const CharT*const p_end, uint64_t& o_result)
{
    // we are parsing without header (0x)
    if (p_begin >= p_end)
    {
        return EResult::k_invalid;
    }

    static constexpr uint64_t maxInt = ~static_cast<uint64_t>(0);
    static constexpr uint64_t maxIntDiv = maxInt >> 4;

    uint64_t accumulator = 0;
    for (const CharT* charIt = p_begin; charIt < p_end; ++charIt)
    {
        const CharT character = *charIt;

        if (!std::iswxdigit(static_cast<wchar_t>(character)))
        {
            return EResult::k_invalid;
        }

        // This is working only because we know that character could be only between [0-9A-Fa-f]
        constexpr CharT setCharacter[] { '0', 'A', 'a' };
        constexpr uint64_t setOffset[] { 0, 10, 10 };
        const int set = (character > '9') + (character > 'F');

        const uint64_t addition = character - setCharacter[set] + setOffset[set];

        if (accumulator > maxIntDiv)
        {
            return EResult::k_overflow;
        }

        accumulator <<= 4;
        accumulator += addition;
    }

    o_result = accumulator;
    return EResult::k_ok;
}

template<typename CharT>
EResult asHexadecimalStrict(const CharT*const p_begin, const CharT*const p_end, uint64_t& o_result)
{
	if (p_begin + 2 > p_end)
	{
		return EResult::k_invalid;
	}

	const CharT firstCharacter = *p_begin;
	const CharT secondCharacter = *(p_begin + 1);
	if (firstCharacter != '0' || (secondCharacter != 'x' && secondCharacter != 'X'))
	{
		return EResult::k_invalid;
	}

	return asHexadecimalBodyStrict(p_begin + 2, p_end, o_result);
}

template<typename CharT>
EResult asOctodecimalBodyStrict(const CharT*const p_begin, const CharT*const p_end, uint64_t& o_result)
{
    // we are parsing without header (which is initial 0, like 01231)
    if (p_begin >= p_end)
    {
        return EResult::k_invalid;
    }

    static constexpr uint64_t maxInt = ~static_cast<uint64_t>(0);
    static constexpr uint64_t maxIntDiv = maxInt >> 3;

    uint64_t accumulator = 0;
    for (const CharT* charIt = p_begin; charIt < p_end; ++charIt)
    {
        const CharT character = *charIt;

        if (character < '0' || character > '7')
        {
            return EResult::k_invalid;
        }

        const uint64_t addition = character - '0';

        if (accumulator > maxIntDiv)
        {
            return EResult::k_overflow;
        }

        accumulator <<= 3;
        accumulator += addition;
    }

    o_result = accumulator;
    return EResult::k_ok;
}

template<typename CharT>
EResult asOctodecimalStrict(const CharT*const p_begin, const CharT*const p_end, uint64_t& o_result)
{
	if (p_begin + 1 > p_end || (*p_begin != '0'))
	{
		return EResult::k_invalid;
	}

	return asOctodecimalBodyStrict(p_begin + 1, p_end, o_result);
}

template<typename CharT>
EResult asBinaryBodyStrict(const CharT*const p_begin, const CharT*const p_end, uint64_t& o_result)
{
    // we are parsing without header (0b)
    if (p_begin >= p_end)
    {
        return EResult::k_invalid;
    }

    static constexpr uint64_t maxInt = ~static_cast<uint64_t>(0);
    static constexpr uint64_t maxIntDiv = maxInt >> 1;

    uint64_t accumulator = 0;
    for (const CharT* charIt = p_begin; charIt < p_end; ++charIt)
    {
        const CharT character = *charIt;

        if (character != '0' && character != '1')
        {
            return EResult::k_invalid;
        }

        const uint64_t addition = character - '0';

        if (accumulator > maxIntDiv)
        {
            return EResult::k_overflow;
        }

        accumulator <<= 1;
        accumulator += addition;
    }

    o_result = accumulator;
    return EResult::k_ok;
}

template<typename CharT>
EResult asBinaryStrict(const CharT*const p_begin, const CharT*const p_end, uint64_t& o_result)
{
	if (p_begin + 2 > p_end)
	{
		return EResult::k_invalid;
	}

	const CharT firstCharacter = *p_begin;
	const CharT secondCharacter = *(p_begin + 1);
	if (firstCharacter != '0' || (secondCharacter != 'b' && secondCharacter != 'B'))
	{
		return EResult::k_invalid;
	}

	return asBinaryBodyStrict(p_begin + 2, p_end, o_result);
}

template<typename CharT>
EResult asDoubleStrict(const CharT*const p_begin, const CharT*const p_end, double& o_result)
{
	if (p_begin >= p_end)
	{
		return EResult::k_invalid;
	}


    const CharT firstCharacter = *p_begin;
    const bool sign = firstCharacter == '-';

	const CharT*const mantissaStartIt = p_begin + (firstCharacter == '-' || firstCharacter == '+');

	/*
	 * Count the number of digits in the mantissa (including the decimal
	 * point), and also locate the decimal point.
	 */

	int decimalPointIndex = -1;
	int mantissaSize = 0;
	
	const CharT* charIt = mantissaStartIt;
	for (; charIt < p_end; ++charIt)
	{
		const CharT character = *charIt;
		if (!std::iswdigit(static_cast<wchar_t>(character)))
		{
			// start of the exponent.
			if (character == 'e' || character == 'E')
			{
				break;
			}

			// If this is not a dicimal point, or our decimal point not the first, the float is invalid.
			const bool firstDecimalPoint = character == '.' && decimalPointIndex == -1;
			if (!firstDecimalPoint)
			{
				return EResult::k_invalid;
			}

			decimalPointIndex = mantissaSize;
		}

		++mantissaSize;
	}

	// If there were no digits in the mantissa, the value is invalid
	if (mantissaSize == 0)
	{
		return EResult::k_invalid;
	}

	// Just to track the start of the exponent. Could equal to p_end.
	const CharT*const exponentStartIt = charIt;

	/*
	 * Now suck up the digits in the mantissa. Use two integers to collect 9
	 * digits each (this is faster than using floating-point). If the mantissa
	 * has more than 18 digits, ignore the extras, since they can't affect the
	 * value anyway.
	 */
	if (decimalPointIndex == -1) 
	{
		decimalPointIndex = mantissaSize;
	} 
	else 
	{
		mantissaSize -= 1; /* One of the digits was the point. */
	}

	/* Exponent that derives from the fractional
	 * part. Under normal circumstances, it is
	 * the negative of the number of digits in F.
	 * However, if I is very long, the last digits
	 * of I get dropped (otherwise a long I with a
	 * large negative exponent could cause an
	 * unnecessary overflow on I alone). In this
	 * case, fracExp is incremented one for each
	 * dropped digit. */
	if (mantissaSize > 18)
	{
		mantissaSize = 18;
	}

	const int fractionalExponent = decimalPointIndex - mantissaSize;
	
	double fraction = [mantissaSize, mantissaStartIt]()
	{
		int localMantissaSize = mantissaSize;
		const CharT* charIt = mantissaStartIt;

		int32_t frac1 = 0;
		while (localMantissaSize > 9) 
		{
			const CharT character = *charIt;
			++charIt;

			if (character == '.')
			{
				continue;
			}

			frac1 = 10 * frac1 + (character - '0');
			--localMantissaSize;
		}

		int32_t frac2 = 0;
		while (localMantissaSize > 0)
		{
			const CharT character = *charIt;
			++charIt;

			if (character == '.')
			{
				continue;
			}

			frac2 = 10 * frac2 + (character - '0');
			--localMantissaSize;
		}

		return (1.0e9 * static_cast<double>(frac1)) + static_cast<double>(frac2);
	}();

	/*
	 * Skim off the exponent.
	 */

	int exponent = 0;
	bool exponentSign = false;
	// The only way we got here in first place is if we either didn't get exponent and all characters till p_end was valid,
	// or if we have struck 'e' and exited the mantissa loop
	if (exponentStartIt != p_end)
	{
		// past the 'e'
		const CharT* exponentCharIt = exponentStartIt + 1;
		const CharT signCharacter = *exponentCharIt;
		
		if (signCharacter == '-' || signCharacter == '+')
		{
			exponentSign = signCharacter == '-';
			++exponentCharIt;
		}

		if (exponentCharIt >= p_end)
		{
			return EResult::k_invalid;
		}

		while (exponentCharIt < p_end)
		{
			const CharT character = *exponentCharIt;
			if (!std::iswdigit(static_cast<wchar_t>(character)))
			{
				return EResult::k_invalid;
			}

			exponent = exponent * 10 + (character - '0');
			++exponentCharIt;
		}
	}
	if (exponentSign)
	{
		exponent = fractionalExponent - exponent;
	}
	else
	{
		exponent = fractionalExponent + exponent;
	}

	/*
	 * Generate a floating-point number that represents the exponent. Do this
	 * by processing the exponent one bit at a time to combine many powers of
	 * 2 of 10. Then combine the exponent with the fraction.
	 */

	if (exponent < 0) 
	{
		exponentSign = true;
		exponent = -exponent;
	} 
	else 
	{
		exponentSign = false;
	}

	/* Largest possible base 10 exponent. Any
	 * exponent larger than this will already
	 * produce underflow or overflow, so there's
	 * no need to worry about additional digits. */
	static constexpr int k_maxExponent = 307;
	/* Table giving binary powers of 10. Entry
	 * is 10^2^i. Used to convert decimal
	 * exponents into floating-point numbers. */
	static const double k_powersOf10[]
	{
		10.,
		100.,
		1.0e4,
		1.0e8,
		1.0e16,
		1.0e32,
		1.0e64,
		1.0e128,
		1.0e256
	};

	if (exponent > k_maxExponent) 
	{
		return EResult::k_overflow;
	}

	double doubleExponent = 1.0;
	for (const double* powerIt = k_powersOf10; exponent != 0; exponent >>= 1, ++powerIt)
	{
		if (exponent & 1) 
		{
			doubleExponent *= *powerIt;
		}
	}
	if (exponentSign) 
	{
		fraction /= doubleExponent;
	} 
	else 
	{
		fraction *= doubleExponent;
	}

	o_result = sign 
		? -fraction
		: fraction;

	return EResult::k_ok;
}

} // namespace str_parsing
} // namespace bwn