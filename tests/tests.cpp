#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <string-parsing/algorithms.hpp>

namespace tests
{

template<typename CharT>
static constexpr CharT* findStrTermination(CharT* p_str)
{
	while (*p_str)
	{
		++p_str;
	}

	return p_str;
}

static bool compareDouble(const double p_left, const double p_right)
{
    return std::abs(p_left - p_right) < std::abs(std::min(p_left, p_right)) * std::numeric_limits<double>::epsilon();
}

}

#define BWN_TO_STRING(x) #x
#define BWN_STRINGIFY(x) BWN_TO_STRING(x)

TEST_CASE("Unsigned int strict")
{
    SECTION("Basic conversion")
    {
        uint64_t value = 0;
        static constexpr char k_string[] { "12345" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asDecimalStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(value == 12345);
    }

    SECTION("Almost overflow check")
    {
        uint64_t value = 0;
        static constexpr char k_string[] { "18446744073709551615" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asDecimalStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(value == 18446744073709551615ull);
    }

    SECTION("Overflow check")
    {
        uint64_t value = 0;
        static constexpr char k_string[] { "18446744073709551616" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asDecimalStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_overflow);
    }
}

TEST_CASE("Signed int strict")
{
    SECTION("Basic conversion")
    {
        int64_t value = 0;
        static constexpr char k_string[] { "12345" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asDecimalStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(value == 12345);
    }

    SECTION("Positive signed conversion")
    {
        int64_t value = 0;
        static constexpr char k_string[] { "+12345" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asDecimalStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(value == 12345);
    }

    SECTION("Negative signed conversion")
    {
        int64_t value = 0;
        static constexpr char k_string[] { "-12345" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asDecimalStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(value == -12345);
    }

    SECTION("Positive signed almost overflow check")
    {
        int64_t value = 0;
        static constexpr char k_string[] { "+9223372036854775807" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asDecimalStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(value == 9223372036854775807ll);
    }

    SECTION("Positive signed overflow check")
    {
        int64_t value = 0;
        static constexpr char k_string[] { "+9223372036854775808" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asDecimalStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_overflow);
    }

    SECTION("Negative signed almost underflow check")
    {
        int64_t value = 0;
        static constexpr char k_string[] { "-9223372036854775807" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asDecimalStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(value == -9223372036854775807ll);
    }

    SECTION("Negative signed underflow check")
    {
        int64_t value = 0;
        static constexpr char k_string[] { "-9223372036854775808" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asDecimalStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_underflow);
    }
}
TEST_CASE("Hexadicimal strict")
{
    SECTION("Basic uppercase conversion")
    {
        uint64_t value = 0;
        static constexpr char k_string[] { "0XDEADBEAF" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asHexadecimalStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(value == 0xdeadbeaf);
    }

    SECTION("Basic lover case conversion")
    {
        uint64_t value = 0;
        static constexpr char k_string[] { "0xdeadbeaf" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asHexadecimalStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(value == 0xdeadbeaf);
    }

    SECTION("Almost overflow check")
    {
        uint64_t value = 0;
        static constexpr char k_string[] { "0xffffFFFFffffFFFF" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asHexadecimalStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(value == 0xffffFFFFffffFFFF);
    }

    SECTION("Overflow check")
    {
        uint64_t value = 0;
        static constexpr char k_string[] { "0x1ffffFFFFffffFFFF" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asHexadecimalStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_overflow);
    }
}

TEST_CASE("Octodicimal strict")
{
    SECTION("Basic conversion")
    {
        uint64_t value = 0;
        static constexpr char k_string[] { "01234567" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asOctodecimalStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(value == 01234567);
    }

    SECTION("Almost overflow check")
    {
        uint64_t value = 0;
        static constexpr char k_string[] { "01777777777777777777777" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asOctodecimalStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(value == 01777777777777777777777);
    }

    SECTION("Overflow check")
    {
        uint64_t value = 0;
        static constexpr char k_string[] { "02000000000000000000000" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asOctodecimalStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_overflow);
    }
}

TEST_CASE("Binary strict")
{
    SECTION("Basic conversion")
    {
        uint64_t value = 0;
        static constexpr char k_string[] { "0b101010" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asBinaryStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(value == 0b101010);
    }

    SECTION("Almost overflow check")
    {
        uint64_t value = 0;
        static constexpr char k_string[] { "0b1111111111111111111111111111111111111111111111111111111111111111" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asBinaryStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(value == 0b1111111111111111111111111111111111111111111111111111111111111111);
    }

    SECTION("Overflow check")
    {
        uint64_t value = 0;
        static constexpr char k_string[] { "0b11111111111111111111111111111111111111111111111111111111111111111" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asBinaryStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_overflow);
    }
}

TEST_CASE("Float strict")
{
    SECTION("Basic conversion")
    {
        double value = 0.0;
        static constexpr char k_string[] { "123.123" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asDoubleStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(value == 123.123);
    }

    SECTION("Positive double max check")
    {
        double value = 0;
        static constexpr char k_string[] { BWN_STRINGIFY(DBL_MAX) };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asDoubleStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(tests::compareDouble(value, DBL_MAX));
    }

    SECTION("Positive double min check")
    {
        double value = 0;
        static constexpr char k_string[] { "2.2250738585072014e-270" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asDoubleStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(tests::compareDouble(value, 2.2250738585072014e-270));
    }

    SECTION("Negative double max check")
    {
        double value = 0;
        static constexpr char k_string[] { "-" BWN_STRINGIFY(DBL_MAX) };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asDoubleStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(tests::compareDouble(value, -DBL_MAX));
    }

    SECTION("Negative double min check")
    {
        double value = 0;
        static constexpr char k_string[] { "-2.2250738585072014e-270" };
        const char*const beginIt = std::begin(k_string);
        const char*const endIt = tests::findStrTermination(beginIt);

        const bwn::str_parsing::EResult result = bwn::str_parsing::asDoubleStrict(beginIt, endIt, value);
        REQUIRE(result == bwn::str_parsing::EResult::k_ok);
        REQUIRE(tests::compareDouble(value, -2.2250738585072014e-270));
    }
}